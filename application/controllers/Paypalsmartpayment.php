<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Paypalsmartpayment extends CI_Controller{

    public function __construct(){
        
        parent::__construct();
        $this->load->helper('url');
        // Load session
         $this->load->library('Session');
        // Load model 
        $this->load->model('Main_model');        
    }

    //list for product
    public function index(){
        // Load view
        $this->load->view('checkout');

    }

    //list for product
    public function checkout(){
        // Load view
        $this->load->view('index');
    
    }

    //controller for storing item into the cart 
    function loadItemCart(){

        $taskData = $this->Main_model->fetch_all_item_cart();
        foreach($taskData->result_array() as $row)
        {
            //storing database value into array
            $data[] = array(
                'name' => $row['name'],
                'unit_amount'=>array(
                    'currency_code' => $row['currency_code'],
                    //value per bottle
                    'value' => $row['value']
                ),
                'quantity' => $row['quantity'],
                'category' => $row['category']
                );
        }
        //converting array value to json
        echo json_encode($data);
        
    }

    function transactioncomplete($orderID){
        $this->load->library('paypal_lib');
        
        $client = $this->paypal_lib->load();
        $response = $client->execute(new OrdersGetRequest($orderId));

        $data = array(
        'order_id'=> $orderID,
        'name'=> $orderID,
        
        );
        echo json_encode($data);
    }

    function insert(){
		$data = array(
            'order_id'=> $this->input->post('order_id'),
            'date_added'=> $this->input->post('date_added'),
            'date_updated'=> $this->input->post('date_added'),
            'timezone'=> $this->input->post('timezone'),
            'name'=> $this->input->post('name'),
            'status'=> $this->input->post('status'),
            'gross'=> $this->input->post('gross'),
            'fee'=> $this->input->post('fee'),
            'net'=> $this->input->post('net'),
            'email'=> $this->input->post('email'),
            'transaction_id'=> $this->input->post('transaction_id'),
            'item_title'=> $this->input->post('item_title'),
            'item_id'=> $this->input->post('item_id'),
            'shipping_cost'=> $this->input->post('shipping_cost'),
            'receipt_id'=> $this->input->post('receipt_id'),
            'address1'=> $this->input->post('address1'),
            'address2'=> $this->input->post('address2'),
            'city'=> $this->input->post('city'),
            'state'=> $this->input->post('state'),
            'country'=> $this->input->post('country'),
            'phone'=> $this->input->post('phone'),
            'zip'=> $this->input->post('zip'),
            'notes'=> $this->input->post('notes'),
            'postage_packaging'=> $this->input->post('postage_packaging'),
            'product_id'=> $this->input->post('product_id'),
            'paypal_account'=> $this->input->post('paypal_account'),
            'payment_gateway'=> $this->input->post('payment_gateway'),
            'page'=> $this->input->post('page'),
            'device'=> $this->input->post('device'),
            'package'=> $this->input->post('package'),
            'ref_id'=> $this->input->post('ref_id'),
            'ref_link'=> $this->input->post('ref_link'),
            'rewarded'=> $this->input->post('rewarded'),
            'coupon_code'=> $this->input->post('coupon_code'),
            'savings'=> $this->input->post('savings')

          
		);
		$this->Main_model->insert_data($data);
	}
        
}