<?php

namespace Sample;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

class Paypal_lib
{
    /**
     * Returns PayPal HTTP client instance with environment that has access
     * credentials context. Use this instance to invoke PayPal APIs, provided the
     * credentials have access.
     */
    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */
    public static function environment()
    {
        $clientId = "AduDmWn77MWVMacWSV6NgvDeD610mXry7LbbZzDGmbk-E8qvAxKR2wq96w3P-6-k8arkEZG9PJWvYrVg";
        $clientSecret = "EDLd3uAYNmX6l5qm0ZgTKwe7ojixgXmj2cLkkSg3rwn3cg9bVBCMIt4mPe4k9SoAJY8-OADtLIHdkBIh";
        return new SandboxEnvironment($clientId, $clientSecret);
    }
}