
<!DOCTYPE html>

<head>
   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="<?= base_url() ?>assets/post_view.css" rel="stylesheet">
</head>

<style>
.paypal-powered-by {
    text-align: center;
    margin: 10px auto;
    height: 14px;
    font-family: PayPal-Sans, HelveticaNeue, sans-serif;
    font-size: 11px;
    display: none;
    font-weight: normal;
    font-style: italic;
    font-stretch: normal;
    color: #7b8388;
    position: relative;
    margin-right: 3px;
    bottom: 3px;
}
  
</style>

<body>
<!--test invoice-->
<div id="invoice-POS">
    
    <center id="top">
      <div class="logo"></div>
      <div class="info"> 
        <h2>Holistic Health Labs</h2>
      </div><!--End Info-->
    </center><!--End InvoiceTop-->
        
    <div id="bot">
					<div id="table">
						<table>
							<tr class="tabletitle">
								<td class="item"><h2>Item</h2></td>
								<td class="Hours"><h2>Qty</h2></td>
								<td class="Rate"><h2>Sub Total</h2></td>
							</tr>
							<tr class="service">
								<td class="tableitem"><p class="itemtext">Energize Greens</p></td>
								<td class="tableitem"><p class="itemtext">3</p></td>
								<td class="tableitem"><p class="itemtext">$105.00</p></td>
							</tr>

                            <tr class="service">
								<td class="tableitem"><p class="itemtext">Myco Ultra</p></td>
								<td class="tableitem"><p class="itemtext">4</p></td>
								<td class="tableitem"><p class="itemtext">$188.00</p></td>
							</tr>

                            <tr class="service">
								<td class="tableitem"><p class="itemtext">Gut Alive</p></td>
								<td class="tableitem"><p class="itemtext">1</p></td>
								<td class="tableitem"><p class="itemtext">$39.97</p></td>
							</tr>

                            <tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Order value</h2></td>
                                <td class="payment"><h2>$332.97</h2></td>
                                <input type='hidden' id='orderValue' name="orderValue" value='332.97'>
							</tr>

							<tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Shipping cost</h2></td>
                                <td class="payment"><h2>$9.95</h2></td>
                                <input type='hidden' id='ShippingCost' name="ShippingCost" value='9.95'>
							</tr>

							<tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Sub total:</h2></td>
                                <td class="payment"><h2>$342.92</h2></td>
                                <input type='hidden' id='subTotal' name="subTotal" value='342.92'>     
							</tr>

						</table>
					</div><!--End Table-->
                <br/>
                <!--Paypal smart payment -->
                <center > 
                    <h2>Subscription Payment</h2>
                </center>
                <div id="paypal-button-container"></div>
                <center > 
                    <h1>Or</h1>
                </center>
                <center >
                    <h2>One Time Payment</h2>
                </center>
                    <div id="paypal-button-container2"></div>
                 
				</div><!--End InvoiceBot-->
  </div><!--End Invoice-->
  <!--Other hidden sample variable-->
    <input type='hidden' id='itemTitle' name="itemTitle" value='Vision Alive Max,Energize Greens'>
    <input type='hidden' id='itemID' name="itemID" value='2,10'>
    <input type='hidden' id='nullValue' name="nullValue" value=' '>
    <input type='hidden' id='gateway' name="gateway" value='PAYPAL'>
    <input type='hidden' id='date' name="date" value='<?php echo date('Y-m-d H:m:s');?>'>

    <!--REST API for subscription payment-->
    <script src="https://www.paypal.com/sdk/js?client-id=AduDmWn77MWVMacWSV6NgvDeD610mXry7LbbZzDGmbk-E8qvAxKR2wq96w3P-6-k8arkEZG9PJWvYrVg&vault=true" data-sdk-integration-source="button-factory"></script>
    <!--REST API for one time payment-->
    <script src="https://www.paypal.com/sdk/js?client-id=AduDmWn77MWVMacWSV6NgvDeD610mXry7LbbZzDGmbk-E8qvAxKR2wq96w3P-6-k8arkEZG9PJWvYrVg&currency=USD"></script>

    <script>
        //subscription payment
        paypal.Buttons({
            //styles of subscription payment
            style: {
                color: 'gold',
                layout: 'vertical',
                label: 'subscribe',
                shape:  'pill',
                height: 40  
            },
            //actions when clicking subcription payment
            createSubscription: function(data, actions) {
                return actions.subscription.create({
                //payment subscription id
                'plan_id': 'P-35079829PE431764RL4WLAYQ'
                });
            },
            //action when the subscription payment is done
            onApprove: function(data, actions) {
                alert(data.subscriptionID);
            }
            //subscription payment display container
        }).render('#paypal-button-container');
        
        //initial value of item order without shipping fee
        var orderValue = document.getElementById("orderValue").value;
        //shipping fee for the item
        var shipping = document.getElementById("ShippingCost").value;
        //total amount on the checkout with shipping cost
        var subTotal = document.getElementById("subTotal").value;
        
        /* amount and breakdown of it's total.
         * note: 
         * value = total value of item including addons
         * shipping {value = initial value of shipping fee}
         * item_total {value = inital value of item without any addons.}
         */ 
        var paypalAmount = {"value":subTotal,"breakdown":{"item_total":{"currency_code":"USD","value":orderValue},"shipping":{"currency_code":"USD","value":shipping}}};

		//item list on the checkout
        var paypalItems;
        fetch('paypalsmartpayment/loadItemCart/')
        .then(res => res.json())
        .then(data => paypalItems = data)
    	
        // one time payment 
        paypal.Buttons({
        // styles of one time payment 
             style: {
                color:  'blue',
                shape:  'pill',
                label:  'pay',
                height: 40
               
            },
            commit:true,
            //actions when clicking one time payment 
            createOrder: function(data, actions) {
                return actions.order.create({
                    //json on purchase units
                    purchase_units: [{
                        //calling json value for amounts
                        amount: paypalAmount,
                        //calling json value for items in the cart
                        items: paypalItems
                    }],
                    headers: {
                        'content-type': 'application/json'
                    },
                });
            },
            //action when the one time payment is done
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    //variables of customer details
                    /*note
                    *nullValue to change on what info needed to inpit
                    */
                    var order_id = details.id;
                    var date_added = document.getElementById("date").value;
                    function seconds_with_leading_zeros(dt) 
                    { 
                    return /\((.*)\)/.exec(new Date().toString())[1];
                    }
                    dt = new Date(); 
                    var timezone = seconds_with_leading_zeros(dt);
                    var name = details.payer.name.given_name +" "+details.payer.name.surname;
                    var status = details.status;
                    var gross = details.purchase_units[0].payments.captures[0].amount.value;
                    var fee = document.getElementById("ShippingCost").value;
                    var nettotal = gross-fee;
                    var net = nettotal;
                    var email = details.payer.email_address;
                    var transaction_id = details.id;
                    var item_title = document.getElementById("itemTitle").value;
                    var item_id = document.getElementById("itemID").value;
                    var shipping_cost = document.getElementById("ShippingCost").value;
                    var receipt_id = details.id;
                    var address1 = details.purchase_units[0].shipping.address.address_line_1;
                    var address2 = details.purchase_units[0].shipping.address.address_line_2;
                    var city = details.purchase_units[0].shipping.address.admin_area_2;
                    var state = details.purchase_units[0].shipping.address.admin_area_1;
                    var country = details.purchase_units[0].shipping.address.country_code;
                    var phone = details.payer.phone;
                    var zip =details.purchase_units[0].shipping.address.postal_code;
                    var notes = document.getElementById("nullValue").value;
                    var postage_packaging = document.getElementById("nullValue").value;
                    var product_id = document.getElementById("nullValue").value;
                    var paypal_account = details.payer.payer_id;
                    var payment_gateway = document.getElementById("gateway").value;
                    var page = document.getElementById("nullValue").value;
                    var device = document.getElementById("nullValue").value;
                    var package = document.getElementById("nullValue").value;
                    var ref_id = document.getElementById("nullValue").value;
                    var ref_link = document.getElementById("nullValue").value;
                    var rewarded = document.getElementById("nullValue").value;
                    var coupon_code = document.getElementById("nullValue").value;
                    var savings = document.getElementById("nullValue").value;
                   //saving value using ajax
                    $.ajax({
                        //api on saving
                        url:"paypalsmartpayment/insert",
                        type:"POST",
                        data:{ order_id:order_id,date_added:date_added,timezone:timezone,name:name,status:status,
                            gross:gross,fee:fee,net:net,email:email,transaction_id:transaction_id,
                            item_title:item_title,item_id:item_id,shipping_cost:shipping_cost,receipt_id:receipt_id,address1:address1,
                            address2:address2,city:city,state:state,country:country,phone:phone,
                            zip:zip,notes:notes,postage_packaging:postage_packaging,product_id:product_id,paypal_account:paypal_account,
                            payment_gateway:payment_gateway,page:page,device:device,package:package,ref_id:ref_id,
                            ref_link:ref_link,rewarded:rewarded,coupon_code:coupon_code,savings:savings
                        },
                        success:function()
                        {              
                            alert('Transaction completed by ' + details.payer.name.given_name + '!');
                            console.log(email);
                        }
                    })      
                }).then(function(res){
                return res.json();
                }).then(function(data){
                console.log(data);
                });
            }
            //one time payment display cointainer
        }).render('#paypal-button-container2');  
    </script>
</body>
    