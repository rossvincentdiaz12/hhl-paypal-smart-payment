
<!DOCTYPE html>

<head>
   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="<?= base_url() ?>assets/post_view.css" rel="stylesheet">
</head>

<style>
.paypal-powered-by {
    text-align: center;
    margin: 10px auto;
    height: 14px;
    font-family: PayPal-Sans, HelveticaNeue, sans-serif;
    font-size: 11px;
    display: none;
    font-weight: normal;
    font-style: italic;
    font-stretch: normal;
    color: #7b8388;
    position: relative;
    margin-right: 3px;
    bottom: 3px;
}
  
</style>

<body>
<!--test invoice-->
<div id="invoice-POS">
    
    <center id="top">
      <div class="logo"></div>
      <div class="info"> 
        <h2>Holistic Health Labs</h2>
      </div><!--End Info-->
    </center><!--End InvoiceTop-->
        
    <div id="bot">
					<div id="table">
						<table>
							<tr class="tabletitle">
								<td class="item"><h2>Item</h2></td>
								<td class="Hours"><h2>Qty</h2></td>
								<td class="Rate"><h2>Sub Total</h2></td>
							</tr>
							<tr class="service">
								<td class="tableitem"><p class="itemtext">Energize Greens</p></td>
								<td class="tableitem"><p class="itemtext">3</p></td>
								<td class="tableitem"><p class="itemtext">$105.00</p></td>
							</tr>

                            <tr class="service">
								<td class="tableitem"><p class="itemtext">Myco Ultra</p></td>
								<td class="tableitem"><p class="itemtext">4</p></td>
								<td class="tableitem"><p class="itemtext">$188.00</p></td>
							</tr>

                            <tr class="service">
								<td class="tableitem"><p class="itemtext">Gut Alive</p></td>
								<td class="tableitem"><p class="itemtext">1</p></td>
								<td class="tableitem"><p class="itemtext">$39.97</p></td>
							</tr>

                            <tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Order value</h2></td>
                                <td class="payment"><h2>$332.97</h2></td>
                                <input type='hidden' id='orderValue' name="orderValue" value='332.97'>
							</tr>

							<tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Shipping cost</h2></td>
                                <td class="payment"><h2>$9.95</h2></td>
                                <input type='hidden' id='ShippingCost' name="ShippingCost" value='9.95'>
							</tr>

							<tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Sub total:</h2></td>
                                <td class="payment"><h2>$342.92</h2></td>
                                <input type='hidden' id='subTotal' name="subTotal" value='342.92'>     
							</tr>

						</table>
					</div><!--End Table-->
                <br/>
                <!--Paypal smart payment -->
                <div id="paypal-button-container2"></div>
                 
				</div><!--End InvoiceBot-->
  </div><!--End Invoice-->
    <!--Other hidden sample variable-->
    <input type='hidden' id='itemTitle' name="itemTitle" value='Vision Alive Max,Energize Greens'>
    <input type='hidden' id='itemID' name="itemID" value='2,10'>
    <input type='hidden' id='nullValue' name="nullValue" value=' '>
    <input type='hidden' id='gateway' name="gateway" value='PAYPAL'>
    <input type='hidden' id='date' name="date" value='<?php echo date('Y-m-d H:m:s');?>'>

    <!--REST API for one time payment-->
    <script src="https://www.paypal.com/sdk/js?client-id=AduDmWn77MWVMacWSV6NgvDeD610mXry7LbbZzDGmbk-E8qvAxKR2wq96w3P-6-k8arkEZG9PJWvYrVg&currency=USD"></script>

    <script>
        //initial value of item order without shipping fee
        var orderValue = document.getElementById("orderValue").value;
        //shipping fee for the item
        var shipping = document.getElementById("ShippingCost").value;
        //total amount on the checkout with shipping cost
        var subTotal = document.getElementById("subTotal").value;
        
        /* amount and breakdown of it's total.
         * note: 
         * value = total value of item including addons
         * shipping {value = initial value of shipping fee}
         * item_total {value = inital value of item without any addons.}
         */ 
        var paypalAmount = {"value":subTotal,"breakdown":{"item_total":{"currency_code":"USD","value":orderValue},"shipping":{"currency_code":"USD","value":shipping}}};

		//item list on the checkout
        var paypalItems;
        fetch('paypalsmartpayment/loadItemCart/')
        .then(res => res.json())
        .then(data => paypalItems = data)
    	
        // one time payment 
        paypal.Buttons({
        // styles of one time payment 
             style: {
                color:  'blue',
                shape:  'pill',
                label:  'pay',
                height: 40
               
            },
            commit:true,
            //actions when clicking one time payment 
            createOrder: function(data, actions) {
                return actions.order.create({
                    //json on purchase units
                    purchase_units: [{
                        //calling json value for amounts
                        amount: paypalAmount,
                        //calling json value for items in the cart
                        items: paypalItems
                    }],
                    headers: {
                        'content-type': 'application/json'
                    },
                });
            },
            //action when the one time payment is done
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    window.location="paypalsmartpayment/transactioncomplete/"+data.orderID;
                }).then(function(res){
                return res.json();
                }).then(function(data){
                console.log(data);
                });
            }
            //one time payment display cointainer
        }).render('#paypal-button-container2');  
    </script>
</body>
    