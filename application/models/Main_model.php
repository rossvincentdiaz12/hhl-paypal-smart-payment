<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Main_model extends CI_Model {

    public function __construct() {
		parent::__construct(); 
	}

	//model for getting all item into cart via json.
	function fetch_all_item_cart(){

		$this->db->order_by('id');
		return $this->db->get('tbl_checkoutitems');
		$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}		

	}

	function insert_data($data)
	{
		$this->db->insert('paypal_details', $data);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	

}