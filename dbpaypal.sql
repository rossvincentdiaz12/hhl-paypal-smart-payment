/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.7.24 : Database - dbpaypal
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbpaypal` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbpaypal`;

/*Table structure for table `paypal_details` */

DROP TABLE IF EXISTS `paypal_details`;

CREATE TABLE `paypal_details` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varbinary(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timezone` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gross` decimal(6,2) DEFAULT NULL,
  `fee` decimal(6,2) DEFAULT NULL,
  `net` decimal(6,2) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_cost` decimal(6,2) DEFAULT '0.00',
  `receipt_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `postage_packaging` decimal(6,2) DEFAULT '0.00',
  `product_id` mediumint(8) unsigned DEFAULT NULL,
  `paypal_account` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_gateway` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` text COLLATE utf8_unicode_ci,
  `device` text COLLATE utf8_unicode_ci,
  `package` text COLLATE utf8_unicode_ci,
  `ref_id` text COLLATE utf8_unicode_ci,
  `ref_link` text COLLATE utf8_unicode_ci,
  `rewarded` int(11) DEFAULT NULL,
  `coupon_code` text COLLATE utf8_unicode_ci,
  `savings` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `name` (`name`),
  KEY `email` (`email`),
  KEY `address1` (`address1`),
  KEY `paypal_account` (`paypal_account`),
  KEY `phone` (`phone`),
  KEY `date_added` (`date_added`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `paypal_details` */

insert  into `paypal_details`(`id`,`order_id`,`date_added`,`date_updated`,`timezone`,`name`,`status`,`gross`,`fee`,`net`,`email`,`transaction_id`,`item_title`,`item_id`,`shipping_cost`,`receipt_id`,`address1`,`address2`,`city`,`state`,`country`,`phone`,`zip`,`notes`,`postage_packaging`,`product_id`,`paypal_account`,`payment_gateway`,`page`,`device`,`package`,`ref_id`,`ref_link`,`rewarded`,`coupon_code`,`savings`) values (1,'34891612DV632480H','2020-08-14 02:08:23','2020-08-14 02:08:23','Philippine','John Doe','COMPLETED','88.90','9.95','78.95','sb-jsezi2792893@personal.example.com','34891612DV632480H','Vision Alive Max,Energize Greens','2,10','9.95','34891612DV632480H','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00'),(2,'57U52986CL7863725','2020-08-14 04:08:27','2020-08-14 04:08:27','Philippine','John Doe','COMPLETED','78.95','9.95','69.00','sb-jsezi2792893@personal.example.com','57U52986CL7863725','Vision Alive Max,Energize Greens','2,10','9.95','57U52986CL7863725','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00'),(3,'4TA64299CT165221S','2020-08-17 01:08:05','2020-08-17 01:08:05','Philippine','John Doe','COMPLETED','88.90','9.95','78.95','sb-jsezi2792893@personal.example.com','4TA64299CT165221S','Vision Alive Max,Energize Greens','2,10','9.95','4TA64299CT165221S','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00'),(4,'08C12212LB118983F','2020-08-17 06:08:07','2020-08-17 06:08:07','Philippine','John Doe','COMPLETED','88.90','9.95','78.95','sb-jsezi2792893@personal.example.com','08C12212LB118983F','Vision Alive Max,Energize Greens','2,10','9.95','08C12212LB118983F','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00'),(5,'59274969787852057','2020-08-18 02:08:03','2020-08-18 02:08:03','Philippine','John Doe','COMPLETED','88.90','9.95','78.95','sb-jsezi2792893@personal.example.com','59274969787852057','Vision Alive Max,Energize Greens','2,10','9.95','59274969787852057','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00'),(6,'82N63309YS399582D','2020-08-18 04:08:05','2020-08-18 04:08:05','Philippine','John Doe','COMPLETED','87.95','9.95','78.00','sb-jsezi2792893@personal.example.com','82N63309YS399582D','Vision Alive Max,Energize Greens','2,10','9.95','82N63309YS399582D','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00'),(7,'1KH93177RX8056712','2020-08-18 05:08:47','2020-08-18 05:08:47','Philippine','John Doe','COMPLETED','342.92','9.95','332.97','sb-jsezi2792893@personal.example.com','1KH93177RX8056712','Vision Alive Max,Energize Greens','2,10','9.95','1KH93177RX8056712','1 Main St',NULL,'San Jose','CA','US',NULL,'95131',' ','0.00',0,'5FWLZGM2847ZN','PAYPAL',' ',' ',' ',' ',' ',0,' ','0.00');

/*Table structure for table `tbl_checkoutitems` */

DROP TABLE IF EXISTS `tbl_checkoutitems`;

CREATE TABLE `tbl_checkoutitems` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `currency_code` varchar(225) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `quantity` int(22) DEFAULT NULL,
  `category` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_checkoutitems` */

insert  into `tbl_checkoutitems`(`id`,`name`,`currency_code`,`value`,`quantity`,`category`) values (3,'Energize Greens','USD',35,3,'PHYSICAL_GOODS'),(4,'Myco Ultra','USD',47,4,'PHYSICAL_GOODS'),(5,'Gut Alive','USD',39.97,1,'PHYSICAL_GOODS');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
